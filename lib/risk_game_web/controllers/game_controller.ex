defmodule RiskGameWeb.GameController do
  use RiskGameWeb, :controller
  alias RiskGame.GameServer

  def new(conn, _params) do
    not_changeset = %{"board" => ""}
    render(conn, :new, not_changeset: not_changeset)
  end

  def create(conn, %{"board" => _board} = options) do
    {:ok, game_id} = GameServer.new(options)
    redirect(conn, to: ~p"/games/#{game_id}")
  end
end
