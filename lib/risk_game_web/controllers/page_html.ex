defmodule RiskGameWeb.PageHTML do
  use RiskGameWeb, :html

  embed_templates "page_html/*"
end
