defmodule RiskGameWeb.GameHTML do
  use RiskGameWeb, :html

  def new(assigns) do
    ~H"""
    <div class="w-1/4">
      <.simple_form :let={f} for={@not_changeset} action={~p"/games"}>
        <h2>Select Map</h2>
        <.input
          type="custom-radio"
          field={f[:board]}
          value="japan"
          label="Japan 4 players"
          id="board_japan"
          required
        />
        <.input
          type="custom-radio"
          field={f[:board]}
          value="earth"
          label="Earth 6 players"
          id="board_earth"
          required
        />
        <:actions>
          <.button>New Game</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end
end
