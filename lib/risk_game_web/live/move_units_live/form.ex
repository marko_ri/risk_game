defmodule RiskGameWeb.MoveUnitsLive.Form do
  use RiskGameWeb, :live_component

  def update(%{pending_move: %{min: min, max: max}} = assigns, socket) do
    default_count = midpoint(min, max)
    {:ok, assign(socket, Map.merge(assigns, %{count: default_count}))}
    # {
    #   :ok,
    #   socket
    #   |> assign(assigns)
    #   |> assign(form: to_form(%{count: default_count}))
    # }
  end

  def handle_event("update_count", %{"count" => count_string}, socket) do
    {count, _} = Integer.parse(count_string)
    # notify_parent({:move_count, count})
    {:noreply, assign(socket, count: count)}
  end

  # defp notify_parent(msg), do: send(self(), {__MODULE__, msg})

  defp midpoint(low, high) do
    low + floor((high - low) / 2)
  end
end
