defmodule RiskGameWeb.GameLive do
  use RiskGameWeb, :live_view
  alias RiskGameWeb.{Endpoint, MoveUnitsLive}
  alias RiskGame.GameServer
  require Logger

  def mount(%{"game_id" => game_id}, _session, socket) do
    if GameServer.alive?(game_id) do
      game_state = GameServer.get_state(game_id)

      if connected?(socket) do
        Endpoint.subscribe("game:" <> game_id)
      end

      {:ok,
       assign(socket,
         game_id: game_id,
         player_id: socket.assigns.current_user.id,
         saved_player_name: socket.assigns.current_user.username,
         player_number: game_state.player_ids[socket.assigns.current_user.id],
         game_state: game_state
       )}
    else
      {:ok,
       socket
       |> put_flash(:error, "Game not found. Start a new one?")
       |> redirect(to: ~p"/games")}
    end
  end

  def handle_event("join", _params, socket) do
    name = socket.assigns.saved_player_name

    case GameServer.authenticate_player(
           socket.assigns.game_id,
           socket.assigns.player_id,
           name
         ) do
      {:ok, player_number} ->
        {:noreply, assign(socket, player_number: player_number)}

      {:error, reason} ->
        Logger.info("Failed to join game: #{reason}")
        {:noreply, socket}
    end
  end

  def handle_event("start", _params, socket) do
    %{game_id: game_id, player_number: player_number} = socket.assigns
    GameServer.action(game_id, player_number, %{type: "start_game"})

    {:noreply, socket}
  end

  def handle_event("place_unit", %{"tile_id" => tile_id_string}, socket) do
    {tile_id, _} = Integer.parse(tile_id_string)
    %{game_id: game_id, player_number: player_number} = socket.assigns
    GameServer.action(game_id, player_number, %{type: "place_unit", tile_id: tile_id})

    {:noreply, socket}
  end

  def handle_event("end_turn", _params, socket) do
    %{game_id: game_id, player_number: player_number} = socket.assigns
    GameServer.action(game_id, player_number, %{type: "end_turn"})

    {:noreply, socket}
  end

  def handle_event("select_tile", %{"tile_id" => tile_id_string}, socket) do
    {tile_id, _} = Integer.parse(tile_id_string)
    %{game_id: game_id, player_number: player_number} = socket.assigns
    GameServer.action(game_id, player_number, %{type: "select_tile", tile_id: tile_id})

    {:noreply, socket}
  end

  def handle_event("unselect_tile", _params, socket) do
    %{game_id: game_id, player_number: player_number} = socket.assigns
    GameServer.action(game_id, player_number, %{type: "unselect_tile"})

    {:noreply, socket}
  end

  def handle_event("attack", %{"tile_id" => tile_id_string}, socket) do
    {tile_id, _} = Integer.parse(tile_id_string)

    %{
      game_id: game_id,
      player_number: player_number,
      game_state: %{selected_tile_id: selected_tile_id}
    } = socket.assigns

    GameServer.action(game_id, player_number, %{
      type: "attack",
      from_id: selected_tile_id,
      to_id: tile_id
    })

    {:noreply, socket}
  end

  def handle_event("start_move", %{"tile_id" => tile_id_string}, socket) do
    {to_tile_id, _} = Integer.parse(tile_id_string)

    %{
      game_id: game_id,
      player_number: player_number,
      game_state: %{selected_tile_id: selected_tile_id}
    } = socket.assigns

    GameServer.action(game_id, player_number, %{
      type: "start_move",
      from_id: selected_tile_id,
      to_id: to_tile_id
    })

    {:noreply, socket}
  end

  def handle_event("move", %{"count" => count_string}, socket) do
    {count, _} = Integer.parse(count_string)
    {:noreply, handle_move_units(socket, count)}
  end

  def handle_event("cancel_move", _params, socket) do
    %{game_id: game_id, player_number: player_number} = socket.assigns
    GameServer.action(game_id, player_number, %{type: "cancel_move"})

    {:noreply, socket}
  end

  def handle_event("noop", _params, socket) do
    {:noreply, socket}
  end

  def handle_info(%{event: "game_updated", payload: %{state: new_state}}, socket) do
    {:noreply, assign(socket, game_state: new_state)}
  end

  def handle_info({MoveUnitsLive.Form, {:move_count, count}}, socket) do
    {:noreply, handle_move_units(socket, count)}
  end

  def handle_move_units(socket, count) do
    %{
      game_id: game_id,
      player_number: player_number,
      game_state: %{pending_move: pending_move}
    } = socket.assigns

    GameServer.action(game_id, player_number, %{
      type: "move",
      from_id: pending_move.from_id,
      to_id: pending_move.to_id,
      count: count
    })

    socket
  end

  defp owner_of_region(region, tiles) do
    region.tile_ids
    |> Enum.map(fn tile_id ->
      tiles[tile_id].owner
    end)
    |> Enum.uniq()
    |> case do
      [owner_id] -> {:ok, owner_id}
      _ -> nil
    end
  end
end
