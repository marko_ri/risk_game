defmodule RiskGameWeb.Layouts do
  use RiskGameWeb, :html

  embed_templates "layouts/*"
end
