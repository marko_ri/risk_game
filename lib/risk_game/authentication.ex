defmodule RiskGame.Authentication do
  alias RiskGame.Player

  def initialize_state(state) do
    Map.put(state, :player_ids, %{})
  end

  def authenticate_player(state, player_id, name) do
    player_num = state.player_ids[player_id]
    match_player_id_with_state_player_num(state, player_id, name, player_num)
  end

  defp match_player_id_with_state_player_num(state, _player_id, _name, player_num)
       when not is_nil(player_num) do
    {:ok, player_num, state}
  end

  defp match_player_id_with_state_player_num(state, player_id, name, _player_num) do
    case state.turn do
      0 ->
        first_available_num =
          state
          |> Player.ai_ids()
          |> List.first()

        match_player_id_with_first_available_num(state, player_id, name, first_available_num)

      _ ->
        {:error, :in_progress}
    end
  end

  defp match_player_id_with_first_available_num(state, player_id, name, first_available_num)
       when not is_nil(first_available_num) do
    state =
      state
      |> put_in([:player_ids, player_id], first_available_num)
      |> Player.update_attributes(first_available_num, %{ai: false, name: name})

    {:ok, first_available_num, state}
  end

  defp match_player_id_with_first_available_num(_state, _player_id, _name, _first_available_id) do
    {:error, :full}
  end
end
