defmodule RiskGame.Region do
  @enforce_keys [:value, :tile_ids]
  defstruct [:value, :tile_ids]

  def initialize_state(state, regions) do
    Map.put(state, :regions, regions)
  end

  def containing_tile_ids(%{regions: regions}, tile_ids) do
    regions
    |> Map.values()
    |> Enum.filter(fn region ->
      region.tile_ids -- tile_ids == []
    end)
  end

  def containing_tile_ids(_, _), do: []
end
