defmodule RiskGame.Repo do
  use Ecto.Repo,
    otp_app: :risk_game,
    adapter: Ecto.Adapters.SQLite3
end
