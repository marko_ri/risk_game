defmodule RiskGame.AI.Passive do
  def take_action(_state), do: %{type: "end_turn"}
end
