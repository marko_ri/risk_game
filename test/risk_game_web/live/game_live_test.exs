defmodule RiskGameWeb.GameLiveTest do
  use RiskGameWeb.ConnCase
  import Phoenix.LiveViewTest

  describe "logged-in user" do
    setup :register_and_log_in_user

    test "redirects when the GameServer is unavailable", %{conn: conn} do
      assert {:error, {:redirect, %{to: "/games"}}} = live(conn, "/games/no-game-here")
    end

    test "connected mount", %{conn: conn} do
      {:ok, game_id} = RiskGame.GameServer.new(%{"board" => "japan"})
      {:ok, _view, html} = live(conn, ~p"/games/#{game_id}")

      assert html =~ ~s(<div class="Game">)
    end

    test "joining and playing a game", %{conn: conn, user: user} do
      {:ok, game_id} = RiskGame.GameServer.new(%{"board" => "japan"})
      {:ok, view, _html} = live(conn, ~p"/games/#{game_id}")

      # Ensure nothing on the board is interactive before the game starts
      refute has_element?(view, ~s([phx-click="place_unit"]))
      refute has_element?(view, ~s([phx-click="select_tile"]))
      refute has_element?(view, ~s([phx-click="start_move"]))
      refute has_element?(view, ~s([phx-click="attack"]))

      assert render(view) =~ "You are currently spectating."

      render_click(element(view, ~s([phx-click="join"])))
      render_click(element(view, ~s([phx-click="start"])))

      assert has_element?(view, ".Player.player-bg-1", user.username)

      assert has_element?(view, ".player-bg-1 span", "3")
      assert render(view) =~ "You have 3 units to place"

      view
      |> first_matching_tile(~s([phx-click="place_unit"]))
      |> render_click

      assert has_element?(view, ".player-bg-1 span", "2")
      assert render(view) =~ "You have 2 units to place"

      view
      |> first_matching_tile(~s([phx-click="place_unit"]))
      |> render_click

      assert has_element?(view, ".player-bg-1 span", "1")
      assert render(view) =~ "You have 1 units to place"

      view
      |> first_matching_tile(~s([phx-click="place_unit"]))
      |> render_click

      refute has_element?(view, ".player-bg-1 span")
      assert render(view) =~ "Select one of your territories"

      view
      |> first_matching_tile(~s([phx-click="select_tile"]))
      |> render_click

      assert has_element?(view, ".Tile.tile-selected")
      assert render(view) =~ "Select an adjacent territory"
    end

    test "the Region Bonuses module", %{conn: conn} do
      {:ok, game_id} = RiskGame.GameServer.new(%{"board" => "japan"})
      {:ok, view, html} = live(conn, ~p"/games/#{game_id}")

      assert html =~ "Region Bonuses"

      game_state = RiskGame.GameServer.get_state(game_id)
      game_state = put_in(game_state.tiles[49].owner, 1)
      game_state = put_in(game_state.tiles[50].owner, 1)
      game_state = put_in(game_state.tiles[58].owner, 1)
      game_state = put_in(game_state.tiles[59].owner, 1)
      send(view.pid, %{event: "game_updated", payload: %{state: game_state}})

      assert has_element?(view, ".region-1.region-ownedby-1")
    end
  end

  # element() requires a single match, but because tiles are assigned randomly,
  # we sometimes can’t uniquely identify a tile for the active player to
  # interact with, so this function returns the result of element() for the
  # first tile matching a selector
  defp first_matching_tile(view, selector) do
    matching_tile_id =
      1..126
      |> Enum.to_list()
      |> Enum.find(fn tile_id ->
        has_element?(view, "#tile_#{tile_id}#{selector}")
      end)

    element(view, "#tile_#{matching_tile_id}")
  end
end
