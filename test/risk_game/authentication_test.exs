defmodule RiskGame.AuthenticationTest do
  use ExUnit.Case, async: true

  alias RiskGame.{Authentication, Player}

  describe "authenticate_player/3" do
    test "when new player joins, they get player_number from the first AI" do
      old_state = %{
        turn: 0,
        players: %{
          1 => %Player{ai: false},
          2 => %Player{ai: RiskGame.AI.Smart}
        },
        player_ids: %{
          123 => 1
        }
      }

      # new player already has player_id (because they are logged in)
      new_player_id = 456

      assert {:ok, new_player_number, new_state} =
               Authentication.authenticate_player(old_state, new_player_id, "Steve")

      assert new_player_number == 2
      assert new_state.players[2].ai == false
      assert new_state.players[2].name == "Steve"
      assert new_state.player_ids[new_player_id] == 2
    end

    test "new player can not join if the game is in progress" do
      old_state = %{
        turn: 1,
        players: %{
          1 => %Player{ai: false},
          2 => %Player{ai: Sengoku.AI.Smart}
        },
        player_ids: %{
          123 => 1
        }
      }

      # new player already has player_id (otherwise can not join the game)
      new_player_id = 456

      assert {:error, :in_progress} =
               Authentication.authenticate_player(old_state, new_player_id, "Steve")
    end

    test "player can not join if there are no AI players left" do
      old_state = %{
        turn: 0,
        players: %{
          1 => %Player{ai: false},
          2 => %Player{ai: false}
        },
        player_ids: %{
          123 => 1,
          789 => 2
        }
      }

      # new player already has player_id (otherwise can not join the game)
      new_player_id = 456

      assert {:error, :full} =
               Authentication.authenticate_player(old_state, new_player_id, "Steve")
    end

    test "if player_id exists, return its matching player_number" do
      player_id = 123

      old_state = %{
        turn: 0,
        players: %{
          1 => %Player{ai: false},
          2 => %Player{ai: RiskGame.AI.Smart}
        },
        player_ids: %{
          player_id => 1
        }
      }

      assert {:ok, 1, ^old_state} =
               Authentication.authenticate_player(old_state, player_id, "Steve")
    end
  end
end
