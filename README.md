# RiskGame

Learning Elixir & Phoenix Liveview, by making a game of risk (this one: [sengoku](https://github.com/stevegrossi/sengoku/))

To start your Phoenix server:

  * Run `mix setup` to install and setup dependencies
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.
